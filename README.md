# Tinkle

Simple theme for a projects and blog website.

Demo [HERE](https://vaskivodev.gitlab.io/tinkle/).

Handmade with a custom version of [Bulmaswatch](https://jenil.github.io/bulmaswatch/)'s [Darkly theme](https://jenil.github.io/bulmaswatch/darkly/). (and [Bulma](https://bulma.io/))

# Instalation

## Dependencies and theme settings

The following should be added to your `.lektorproject` file:

```ini
[theme_settings]
website_name = Website Name (that appears in the site's "HEAD". Change at will)
navbar_config = Section 1=path,Section 2=path,...
commento_host = https://commento-host.com/
contact_email = email
contact_youtube = Youtube Channel Name
contact_twitter = Twitter Username
contact_gitlab = Gitlab Username
google_analytics_id = analytics_id

[packages]
lektor-markdown-excerpt = 0.1
lektor-markdown-highlighter = 0.3.1
```

The `navbar_config` setting configures the theme's navbar.
It's string of comma separated expressions of the sort `<section_name>=<path>` where:
- `section_name` is what appears in the site.
- `path` is the relative path to access the section.

The `commento_host` confiures comment using [Commento](https://www.commento.io/)

## Syntax highlighting

Copy the contents of the `copy_contents_to_project_folder` folder to your project folder.

The file `configs/markdown-highlighter.ini` can be chenged to change the syntax highlighting
theme. Try them out [here](https://pygments.org/demo/).
